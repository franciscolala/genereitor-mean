'use strict';

// Setting up route
angular.module('<<minModuleName>>s').config(['$stateProvider',
  function ($stateProvider) {
    // <<capsModuleName>>s state routing
    $stateProvider
      .state('<<minModuleName>>s', {
        abstract: true,
        url: '/<<minModuleName>>s',
        template: '<ui-view/>'
      })
      .state('<<minModuleName>>s.list', {
        url: '',
        templateUrl: 'modules/<<minModuleName>>s/client/views/list-<<minModuleName>>.client.view.html'
      })
      .state('<<minModuleName>>s.create', {
        url: '/create',
        templateUrl: 'modules/<<minModuleName>>s/client/views/create-<<minModuleName>>.client.view.html',
        data: {
          roles: ['user', 'admin']
        }
      })
      .state('<<minModuleName>>s.view', {
        url: '/:<<minModuleName>>Id',
        templateUrl: 'modules/<<minModuleName>>s/client/views/view-<<minModuleName>>.client.view.html'
      })
      .state('<<minModuleName>>s.edit', {
        url: '/:<<minModuleName>>Id/edit',
        templateUrl: 'modules/<<minModuleName>>s/client/views/edit-<<minModuleName>>.client.view.html',
        data: {
          roles: ['user', 'admin']
        }
      });
  }
]);
