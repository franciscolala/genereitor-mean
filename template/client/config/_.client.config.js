'use strict';

// Configuring the <<capModuleName>> module
angular.module('<<minModuleName>>s').run(['Menus',
  function (Menus) {
    // Add the <<minModuleName>> dropdown item
    Menus.addMenuItem('topbar', {
      title: '<<capModuleName>>',
      state: '<<minModuleName>>s',
      type: 'dropdown',
      roles: ['*']
    });

    // Add the dropdown list item
    Menus.addSubMenuItem('topbar', '<<minModuleName>>s', {
      title: 'List <<capModuleName>>s',
      state: '<<minModuleName>>s.list'
    });

    // Add the dropdown create item
    Menus.addSubMenuItem('topbar', '<<minModuleName>>s', {
      title: 'Create <<capModuleName>>',
      state: '<<minModuleName>>s.create',
      roles: ['user']
    });
  }
]);
