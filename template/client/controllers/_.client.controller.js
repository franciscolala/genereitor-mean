'use strict';

// <<capModuleName>>s controller
angular.module('<<minModuleName>>s').controller('<<capModuleName>>sController', ['$scope', '$stateParams', '$location', 'Authentication', '<<capModuleName>>s',
  function ($scope, $stateParams, $location, Authentication, <<capModuleName>>s) {
    $scope.authentication = Authentication;

    // Create new <<capModuleName>>
    $scope.create = function (isValid) {
      $scope.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', '<<minModuleName>>Form');

        return false;
      }

      // Create new <<capModuleName>> object
      var <<minModuleName>> = new <<capModuleName>>s({
        title: this.title,
        content: this.content
      });

      // Redirect after save
      <<minModuleName>>.$save(function (response) {
        $location.path('<<minModuleName>>s/' + response._id);

        // Clear form fields
        $scope.title = '';
        $scope.content = '';
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };

    // Remove existing <<capModuleName>>
    $scope.remove = function (<<minModuleName>>) {
      if (<<minModuleName>>) {
        <<minModuleName>>.$remove();

        for (var i in $scope.<<minModuleName>>s) {
          if ($scope.<<minModuleName>>s[i] === <<minModuleName>>) {
            $scope.<<minModuleName>>s.splice(i, 1);
          }
        }
      } else {
        $scope.<<minModuleName>>.$remove(function () {
          $location.path('<<minModuleName>>s');
        });
      }
    };

    // Update existing <<capModuleName>>
    $scope.update = function (isValid) {
      $scope.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', '<<minModuleName>>Form');

        return false;
      }

      var <<minModuleName>> = $scope.<<minModuleName>>;

      <<minModuleName>>.$update(function () {
        $location.path('<<minModuleName>>s/' + <<minModuleName>>._id);
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };

    // Find a list of <<capModuleName>>s
    $scope.find = function () {
      $scope.<<minModuleName>>s = <<capModuleName>>s.query();
    };

    // Find existing <<capModuleName>>
    $scope.findOne = function () {
      $scope.<<minModuleName>> = <<capModuleName>>s.get({
        <<minModuleName>>Id: $stateParams.<<minModuleName>>Id
      });
    };
  }
]);
