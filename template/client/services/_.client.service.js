'use strict';

//<<capModuleName>>s service used for communicating with the <<minModuleName>>s REST endpoints
angular.module('<<minModuleName>>s').factory('<<capModuleName>>s', ['$resource',
  function ($resource) {
    return $resource('api/<<minModuleName>>s/:<<minModuleName>>Id', {
      <<minModuleName>>Id: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
]);
