'use strict';

/**
 * Module dependencies.
 */
var <<minModuleName>>sPolicy = require('../policies/<<minModuleName>>.server.policy'),
  <<minModuleName>>s = require('../controllers/<<minModuleName>>.server.controller');

module.exports = function (app) {
  // <<capModuleName>> collection routes
  app.route('/api/<<minModuleName>>s').all(<<minModuleName>>sPolicy.isAllowed)
    .get(<<minModuleName>>s.list)
    .post(<<minModuleName>>s.create);

  // Single article routes
  app.route('/api/<<minModuleName>>s/:<<minModuleName>>Id').all(<<minModuleName>>sPolicy.isAllowed)
    .get(<<minModuleName>>s.read)
    .put(<<minModuleName>>s.update)
    .delete(<<minModuleName>>s.delete);

  // Finish by binding the <<minModuleName>> middleware
  app.param('<<minModuleName>>Id', <<minModuleName>>s.<<minModuleName>>ByID);
};
