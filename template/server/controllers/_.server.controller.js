'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  <<capModuleName>> = mongoose.model('<<capModuleName>>'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/**
 * Create a <<minModuleName>>
 */
exports.create = function (req, res) {
  var <<minModuleName>> = new <<capModuleName>>(req.body);
  <<minModuleName>>.user = req.user;

  <<minModuleName>>.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(<<minModuleName>>);
    }
  });
};

/**
 * Show the current <<minModuleName>>
 */
exports.read = function (req, res) {
  res.json(req.<<minModuleName>>);
};

/**
 * Update a <<minModuleName>>
 */
exports.update = function (req, res) {
  var <<minModuleName>> = req.<<minModuleName>>;

  <<minModuleName>>.title = req.body.title;
  <<minModuleName>>.content = req.body.content;

  <<minModuleName>>.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(<<minModuleName>>);
    }
  });
};

/**
 * Delete an <<minModuleName>>
 */
exports.delete = function (req, res) {
  var <<minModuleName>> = req.<<minModuleName>>;

  <<minModuleName>>.remove(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(<<minModuleName>>);
    }
  });
};

/**
 * List of <<capModuleName>>
 */
exports.list = function (req, res) {
  <<capModuleName>>.find().sort('-created').populate('user', 'displayName').exec(function (err, <<minModuleName>>s) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(<<minModuleName>>s);
    }
  });
};

/**
 * <<capModuleName>> middleware
 */
exports.<<minModuleName>>ByID = function (req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: '<<capModuleName>> is invalid'
    });
  }

  <<capModuleName>>.findById(id).populate('user', 'displayName').exec(function (err, <<minModuleName>>) {
    if (err) {
      return next(err);
    } else if (!<<minModuleName>>) {
      return res.status(404).send({
        message: 'No <<minModuleName>> with that identifier has been found'
      });
    }
    req.<<minModuleName>> = <<minModuleName>>;
    next();
  });
};
