'use strict';

(function () {
  // <<capModuleName>>s Controller Spec
  describe('<<capModuleName>>s Controller Tests', function () {
    // Initialize global variables
    var <<capModuleName>>sController,
      scope,
      $httpBackend,
      $stateParams,
      $location,
      Authentication,
      <<capModuleName>>s,
      mock<<capModuleName>>;

    // The $resource service augments the response object with methods for updating and deleting the resource.
    // If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
    // the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
    // When the toEqualData matcher compares two objects, it takes only object properties into
    // account and ignores methods.
    beforeEach(function () {
      jasmine.addMatchers({
        toEqualData: function (util, customEqualityTesters) {
          return {
            compare: function (actual, expected) {
              return {
                pass: angular.equals(actual, expected)
              };
            }
          };
        }
      });
    });

    // Then we can start by loading the main application module
    beforeEach(module(ApplicationConfiguration.applicationModuleName));

    // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
    // This allows us to inject a service but then attach it to a variable
    // with the same name as the service.
    beforeEach(inject(function ($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_, _Authentication_, _<<capModuleName>>s_) {
      // Set a new global scope
      scope = $rootScope.$new();

      // Point global variables to injected services
      $stateParams = _$stateParams_;
      $httpBackend = _$httpBackend_;
      $location = _$location_;
      Authentication = _Authentication_;
      <<capModuleName>>s = _<<capModuleName>>s_;

      // create mock <<minModuleName>>
      mock<<capModuleName>> = new <<capModuleName>>s({
        _id: '525a8422f6d0f87f0e407a33',
        title: 'An <<capModuleName>> about MEAN',
        content: 'MEAN rocks!'
      });

      // Mock logged in user
      Authentication.user = {
        roles: ['user']
      };

      // Initialize the <<capModuleName>>s controller.
      <<capModuleName>>sController = $controller('<<capModuleName>>sController', {
        $scope: scope
      });
    }));

    it('$scope.find() should create an array with at least one <<minModuleName>> object fetched from XHR', inject(function (<<capModuleName>>s) {
      // Create a sample <<minModuleName>>s array that includes the new <<minModuleName>>
      var sample<<capModuleName>>s = [mock<<capModuleName>>];

      // Set GET response
      $httpBackend.expectGET('api/<<minModuleName>>s').respond(sample<<capModuleName>>s);

      // Run controller functionality
      scope.find();
      $httpBackend.flush();

      // Test scope value
      expect(scope.<<minModuleName>>s).toEqualData(sample<<capModuleName>>s);
    }));

    it('$scope.findOne() should create an array with one <<minModuleName>> object fetched from XHR using a <<minModuleName>>Id URL parameter', inject(function (<<capModuleName>>s) {
      // Set the URL parameter
      $stateParams.<<minModuleName>>Id = mock<<capModuleName>>._id;

      // Set GET response
      $httpBackend.expectGET(/api\/<<minModuleName>>s\/([0-9a-fA-F]{24})$/).respond(mock<<capModuleName>>);

      // Run controller functionality
      scope.findOne();
      $httpBackend.flush();

      // Test scope value
      expect(scope.<<minModuleName>>).toEqualData(mock<<capModuleName>>);
    }));

    describe('$scope.create()', function () {
      var sample<<capModuleName>>PostData;

      beforeEach(function () {
        // Create a sample <<minModuleName>> object
        sample<<capModuleName>>PostData = new <<capModuleName>>s({
          title: 'An <<capModuleName>> about MEAN',
          content: 'MEAN rocks!'
        });

        // Fixture mock form input values
        scope.title = 'An <<capModuleName>> about MEAN';
        scope.content = 'MEAN rocks!';

        spyOn($location, 'path');
      });

      it('should send a POST request with the form input values and then locate to new object URL', inject(function (<<capModuleName>>s) {
        // Set POST response
        $httpBackend.expectPOST('api/<<minModuleName>>s', sample<<capModuleName>>PostData).respond(mock<<capModuleName>>);

        // Run controller functionality
        scope.create(true);
        $httpBackend.flush();

        // Test form inputs are reset
        expect(scope.title).toEqual('');
        expect(scope.content).toEqual('');

        // Test URL redirection after the <<minModuleName>> was created
        expect($location.path.calls.mostRecent().args[0]).toBe('<<minModuleName>>s/' + mock<<capModuleName>>._id);
      }));

      it('should set scope.error if save error', function () {
        var errorMessage = 'this is an error message';
        $httpBackend.expectPOST('api/<<minModuleName>>s', sample<<capModuleName>>PostData).respond(400, {
          message: errorMessage
        });

        scope.create(true);
        $httpBackend.flush();

        expect(scope.error).toBe(errorMessage);
      });
    });

    describe('$scope.update()', function () {
      beforeEach(function () {
        // Mock <<minModuleName>> in scope
        scope.<<minModuleName>> = mock<<capModuleName>>;
      });

      it('should update a valid <<minModuleName>>', inject(function (<<capModuleName>>s) {
        // Set PUT response
        $httpBackend.expectPUT(/api\/<<minModuleName>>s\/([0-9a-fA-F]{24})$/).respond();

        // Run controller functionality
        scope.update(true);
        $httpBackend.flush();

        // Test URL location to new object
        expect($location.path()).toBe('/<<minModuleName>>s/' + mock<<capModuleName>>._id);
      }));

      it('should set scope.error to error response message', inject(function (<<capModuleName>>s) {
        var errorMessage = 'error';
        $httpBackend.expectPUT(/api\/<<minModuleName>>s\/([0-9a-fA-F]{24})$/).respond(400, {
          message: errorMessage
        });

        scope.update(true);
        $httpBackend.flush();

        expect(scope.error).toBe(errorMessage);
      }));
    });

    describe('$scope.remove(<<minModuleName>>)', function () {
      beforeEach(function () {
        // Create new <<minModuleName>>s array and include the <<minModuleName>>
        scope.<<minModuleName>>s = [mock<<capModuleName>>, {}];

        // Set expected DELETE response
        $httpBackend.expectDELETE(/api\/<<minModuleName>>s\/([0-9a-fA-F]{24})$/).respond(204);

        // Run controller functionality
        scope.remove(mock<<capModuleName>>);
      });

      it('should send a DELETE request with a valid <<minModuleName>>Id and remove the <<minModuleName>> from the scope', inject(function (<<capModuleName>>s) {
        expect(scope.<<minModuleName>>s.length).toBe(1);
      }));
    });

    describe('scope.remove()', function () {
      beforeEach(function () {
        spyOn($location, 'path');
        scope.<<minModuleName>> = mock<<capModuleName>>;

        $httpBackend.expectDELETE(/api\/<<minModuleName>>s\/([0-9a-fA-F]{24})$/).respond(204);

        scope.remove();
        $httpBackend.flush();
      });

      it('should redirect to <<minModuleName>>s', function () {
        expect($location.path).toHaveBeenCalledWith('<<minModuleName>>s');
      });
    });
  });
}());
