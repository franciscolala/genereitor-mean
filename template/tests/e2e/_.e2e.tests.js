'use strict';

describe('<<capModuleName>>s E2E Tests:', function () {
  describe('Test <<minModuleName>>s page', function () {
    it('Should report missing credentials', function () {
      browser.get('http://localhost:3001/<<minModuleName>>s');
      expect(element.all(by.repeater('<<minModuleName>> in <<minModuleName>>s')).count()).toEqual(0);
    });
  });
});
