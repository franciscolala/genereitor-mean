'use strict';

var should = require('should'),
  request = require('supertest'),
  path = require('path'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  <<capModuleName>> = mongoose.model('<<capModuleName>>'),
  express = require(path.resolve('./config/lib/express'));

/**
 * Globals
 */
var app, agent, credentials, user, <<minModuleName>>;

/**
 * <<capModuleName>> routes tests
 */
describe('<<capModuleName>> CRUD tests', function () {

  before(function (done) {
    // Get application
    app = express.init(mongoose);
    agent = request.agent(app);

    done();
  });

  beforeEach(function (done) {
    // Create user credentials
    credentials = {
      username: 'username',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create a new user
    user = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'test@test.com',
      username: credentials.username,
      password: credentials.password,
      provider: 'local'
    });

    // Save a user to the test db and create new <<minModuleName>>
    user.save(function () {
      <<minModuleName>> = {
        title: '<<capModuleName>> Title',
        content: '<<capModuleName>> Content'
      };

      done();
    });
  });

  it('should be able to save an <<minModuleName>> if logged in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new <<minModuleName>>
        agent.post('/api/<<minModuleName>>s')
          .send(<<minModuleName>>)
          .expect(200)
          .end(function (<<minModuleName>>SaveErr, <<minModuleName>>SaveRes) {
            // Handle <<minModuleName>> save error
            if (<<minModuleName>>SaveErr) {
              return done(<<minModuleName>>SaveErr);
            }

            // Get a list of <<minModuleName>>s
            agent.get('/api/<<minModuleName>>s')
              .end(function (<<minModuleName>>sGetErr, <<minModuleName>>sGetRes) {
                // Handle <<minModuleName>> save error
                if (<<minModuleName>>sGetErr) {
                  return done(<<minModuleName>>sGetErr);
                }

                // Get <<minModuleName>>s list
                var <<minModuleName>>s = <<minModuleName>>sGetRes.body;

                // Set assertions
                (<<minModuleName>>s[0].user._id).should.equal(userId);
                (<<minModuleName>>s[0].title).should.match('<<capModuleName>> Title');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to save an <<minModuleName>> if not logged in', function (done) {
    agent.post('/api/<<minModuleName>>s')
      .send(<<minModuleName>>)
      .expect(403)
      .end(function (<<minModuleName>>SaveErr, <<minModuleName>>SaveRes) {
        // Call the assertion callback
        done(<<minModuleName>>SaveErr);
      });
  });

  it('should not be able to save an <<minModuleName>> if no title is provided', function (done) {
    // Invalidate title field
    <<minModuleName>>.title = '';

    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new <<minModuleName>>
        agent.post('/api/<<minModuleName>>s')
          .send(<<minModuleName>>)
          .expect(400)
          .end(function (<<minModuleName>>SaveErr, <<minModuleName>>SaveRes) {
            // Set message assertion
            (<<minModuleName>>SaveRes.body.message).should.match('Title cannot be blank');

            // Handle <<minModuleName>> save error
            done(<<minModuleName>>SaveErr);
          });
      });
  });

  it('should be able to update an <<minModuleName>> if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new <<minModuleName>>
        agent.post('/api/<<minModuleName>>s')
          .send(<<minModuleName>>)
          .expect(200)
          .end(function (<<minModuleName>>SaveErr, <<minModuleName>>SaveRes) {
            // Handle <<minModuleName>> save error
            if (<<minModuleName>>SaveErr) {
              return done(<<minModuleName>>SaveErr);
            }

            // Update <<minModuleName>> title
            <<minModuleName>>.title = 'WHY YOU GOTTA BE SO MEAN?';

            // Update an existing <<minModuleName>>
            agent.put('/api/<<minModuleName>>s/' + <<minModuleName>>SaveRes.body._id)
              .send(<<minModuleName>>)
              .expect(200)
              .end(function (<<minModuleName>>UpdateErr, <<minModuleName>>UpdateRes) {
                // Handle <<minModuleName>> update error
                if (<<minModuleName>>UpdateErr) {
                  return done(<<minModuleName>>UpdateErr);
                }

                // Set assertions
                (<<minModuleName>>UpdateRes.body._id).should.equal(<<minModuleName>>SaveRes.body._id);
                (<<minModuleName>>UpdateRes.body.title).should.match('WHY YOU GOTTA BE SO MEAN?');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should be able to get a list of <<minModuleName>>s if not signed in', function (done) {
    // Create new <<minModuleName>> model instance
    var <<minModuleName>>Obj = new <<capModuleName>>(<<minModuleName>>);

    // Save the <<minModuleName>>
    <<minModuleName>>Obj.save(function () {
      // Request <<minModuleName>>s
      request(app).get('/api/<<minModuleName>>s')
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Array).and.have.lengthOf(1);

          // Call the assertion callback
          done();
        });

    });
  });

  it('should be able to get a single <<minModuleName>> if not signed in', function (done) {
    // Create new <<minModuleName>> model instance
    var <<minModuleName>>Obj = new <<capModuleName>>(<<minModuleName>>);

    // Save the <<minModuleName>>
    <<minModuleName>>Obj.save(function () {
      request(app).get('/api/<<minModuleName>>s/' + <<minModuleName>>Obj._id)
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Object).and.have.property('title', <<minModuleName>>.title);

          // Call the assertion callback
          done();
        });
    });
  });

  it('should return proper error for single <<minModuleName>> with an invalid Id, if not signed in', function (done) {
    // test is not a valid mongoose Id
    request(app).get('/api/<<minModuleName>>s/test')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', '<<minModuleName>> is invalid');

        // Call the assertion callback
        done();
      });
  });

  it('should return proper error for single <<minModuleName>> which doesnt exist, if not signed in', function (done) {
    // This is a valid mongoose Id but a non-existent <<minModuleName>>
    request(app).get('/api/<<minModuleName>>s/559e9cd815f80b4c256a8f41')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'No <<minModuleName>> with that identifier has been found');

        // Call the assertion callback
        done();
      });
  });

  it('should be able to delete an <<minModuleName>> if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new <<minModuleName>>
        agent.post('/api/<<minModuleName>>s')
          .send(<<minModuleName>>)
          .expect(200)
          .end(function (<<minModuleName>>SaveErr, <<minModuleName>>SaveRes) {
            // Handle <<minModuleName>> save error
            if (<<minModuleName>>SaveErr) {
              return done(<<minModuleName>>SaveErr);
            }

            // Delete an existing <<minModuleName>>
            agent.delete('/api/<<minModuleName>>s/' + <<minModuleName>>SaveRes.body._id)
              .send(<<minModuleName>>)
              .expect(200)
              .end(function (<<minModuleName>>DeleteErr, <<minModuleName>>DeleteRes) {
                // Handle <<minModuleName>> error error
                if (<<minModuleName>>DeleteErr) {
                  return done(<<minModuleName>>DeleteErr);
                }

                // Set assertions
                (<<minModuleName>>DeleteRes.body._id).should.equal(<<minModuleName>>SaveRes.body._id);

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to delete an <<minModuleName>> if not signed in', function (done) {
    // Set <<minModuleName>> user
    <<minModuleName>>.user = user;

    // Create new <<minModuleName>> model instance
    var <<minModuleName>>Obj = new <<capModuleName>>(<<minModuleName>>);

    // Save the <<minModuleName>>
    <<minModuleName>>Obj.save(function () {
      // Try deleting <<minModuleName>>
      request(app).delete('/api/<<minModuleName>>s/' + <<minModuleName>>Obj._id)
        .expect(403)
        .end(function (<<minModuleName>>DeleteErr, <<minModuleName>>DeleteRes) {
          // Set message assertion
          (<<minModuleName>>DeleteRes.body.message).should.match('User is not authorized');

          // Handle <<minModuleName>> error error
          done(<<minModuleName>>DeleteErr);
        });

    });
  });

  it('should be able to get a single <<minModuleName>> that has an orphaned user reference', function (done) {
    // Create orphan user creds
    var _creds = {
      username: 'orphan',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create orphan user
    var _orphan = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'orphan@test.com',
      username: _creds.username,
      password: _creds.password,
      provider: 'local'
    });

    _orphan.save(function (err, orphan) {
      // Handle save error
      if (err) {
        return done(err);
      }

      agent.post('/api/auth/signin')
        .send(_creds)
        .expect(200)
        .end(function (signinErr, signinRes) {
          // Handle signin error
          if (signinErr) {
            return done(signinErr);
          }

          // Get the userId
          var orphanId = orphan._id;

          // Save a new <<minModuleName>>
          agent.post('/api/<<minModuleName>>s')
            .send(<<minModuleName>>)
            .expect(200)
            .end(function (<<minModuleName>>SaveErr, <<minModuleName>>SaveRes) {
              // Handle <<minModuleName>> save error
              if (<<minModuleName>>SaveErr) {
                return done(<<minModuleName>>SaveErr);
              }

              // Set assertions on new <<minModuleName>>
              (<<minModuleName>>SaveRes.body.title).should.equal(<<minModuleName>>.title);
              should.exist(<<minModuleName>>SaveRes.body.user);
              should.equal(<<minModuleName>>SaveRes.body.user._id, orphanId);

              // force the <<minModuleName>> to have an orphaned user reference
              orphan.remove(function () {
                // now signin with valid user
                agent.post('/api/auth/signin')
                  .send(credentials)
                  .expect(200)
                  .end(function (err, res) {
                    // Handle signin error
                    if (err) {
                      return done(err);
                    }

                    // Get the <<minModuleName>>
                    agent.get('/api/<<minModuleName>>s/' + <<minModuleName>>SaveRes.body._id)
                      .expect(200)
                      .end(function (<<minModuleName>>InfoErr, <<minModuleName>>InfoRes) {
                        // Handle <<minModuleName>> error
                        if (<<minModuleName>>InfoErr) {
                          return done(<<minModuleName>>InfoErr);
                        }

                        // Set assertions
                        (<<minModuleName>>InfoRes.body._id).should.equal(<<minModuleName>>SaveRes.body._id);
                        (<<minModuleName>>InfoRes.body.title).should.equal(<<minModuleName>>.title);
                        should.equal(<<minModuleName>>InfoRes.body.user, undefined);

                        // Call the assertion callback
                        done();
                      });
                  });
              });
            });
        });
    });
  });

  afterEach(function (done) {
    User.remove().exec(function () {
      <<capModuleName>>.remove().exec(done);
    });
  });
});
