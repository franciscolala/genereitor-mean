#!/usr/bin/env node
'use strict';

process.bin = process.title = 'genereitor-mean';

var inquirer = require('inquirer'),
    fs = require('fs'),
    path = require('path'),
    moduleName = '';


String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

var walk = function(dir, done) {
    var results = [];
    fs.readdir(dir, function(err, list) {
        if (err) return done(err);
        var pending = list.length;
        if (!pending) return done(null, results);
        list.forEach(function(file) {
            file = path.resolve(dir, file);
            fs.stat(file, function(err, stat) {
                if (stat && stat.isDirectory()) {
                    var parent = path.basename(path.dirname(file));
                    if (!fs.existsSync('modules/' + moduleName + 's/' + ((parent !== 'template') ? parent : '') + '/' + path.basename(file))){
                        fs.mkdirSync('modules/' + moduleName + 's/' + ((parent !== 'template') ? parent : '') + '/' + path.basename(file));
                    }
                    walk(file, function(err, res) {
                        results = results.concat(res);
                        if (!--pending) done(null, results);
                    });
                } else {
                    fs.readFile(file, 'utf8', function (err,data) {
                        if (err) {
                            return console.log(err);
                        }

                        var result = data.replace(/<<minModuleName>>/gi, moduleName).replace(/<<capModuleName>>/gi, moduleName.capitalize());

                        var suffix = path.basename(file).split('_')[1];
                        var prefix = path.basename(file).split('_')[0];
                        var parent = path.dirname(file).split('template\\')[1];
                        
                        fs.writeFile('modules/' + moduleName + 's/' + parent + '/' + ((prefix !== '') ? prefix : '') + moduleName + suffix, result, 'utf8', function (err) {
                            if (err) return console.log(err);
                        });
                    });
                    results.push(file);
                    if (!--pending) done(null, results);
                }
            });
        });
    });
};



function init(){
    inquirer.prompt(
        [
            {
                type: "input",
                name: "moduleName",
                message: "Nombre del modulo"
            }
        ],
        function(answers){
            moduleName = answers.moduleName;
            if (!fs.existsSync('modules/' + moduleName + 's')){
                fs.mkdirSync('modules/' + moduleName + 's');
            }
			walk(process.env.APPDATA + '/npm/node_modules/genereitor-mean/template', function(err, results) {
                if (err) throw err;
            });
        }
    );
}

init();

module.exports = init;